package br.com.crud.bo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

import br.com.crud.dao.EmpresaDAO;
import br.com.crud.model.Empresa;
import br.com.crud.model.Filial;
import br.com.crud.model.Matriz;

public class EmpresaManagedBean implements java.io.Serializable {
	private static Logger log = Logger.getLogger(EmpresaManagedBean.class);
	private static final long serialVersionUID = 1L;
	private String selectedCNPJ;
	private boolean matrizFilial;
	private String msg;
	private Empresa empresa;
	private String tipo;
	private List<Empresa> empresas;
	private List<Matriz> matrizes;
	
	private static final String INDEX = "index";
	private static final String INSERT = "insert";
	private static final String UPDATE = "update";

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}

	public List<Matriz> getMatrizes() {
		return matrizes;
	}

	public void setMatrizes(List<Matriz> matrizes) {
		this.matrizes = matrizes;
	}

	public String getSelectedCNPJ() {
		return selectedCNPJ;
	}

	public void setSelectedCNPJ(String cnpj) {
		this.selectedCNPJ = cnpj;
	}

	public boolean isMatrizFilial() {
		return matrizFilial;
	}

	public void setMatrizFilial(boolean matrizFilial) {
		this.matrizFilial = matrizFilial;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String message) {
		this.msg = message;
	}

	@PostConstruct
	public void setup() {
		this.matrizFilial = true;
		this.empresa = new Matriz();
		this.setTipo("Matriz");
		this.empresas = new ArrayList<Empresa>();
		this.matrizes = new ArrayList<Matriz>();
	}
	
	public void selecionarEmpresa() {
		if(this.matrizFilial) {
			this.empresa = null;
			this.empresa = new Matriz();
			this.setTipo("Matriz");
		}
		else {
			this.empresa = null;
			this.empresa = new Filial();
			this.setTipo("Filial");
		}
	}
	
	public void limparEmpresa() {
		if(this.empresa != null) {
			log.info("Limpando empresa");
			this.empresa.setRazaoSocial("");
			this.empresa.setCNPJ("");
			this.empresa.setCNPJMatriz("");
			this.empresa.setComplemento("");
			this.empresa.setLogradouro("");
			this.empresa.setMunicipio("");
			this.empresa.setUf("");
			this.empresa.setNumero(null);
			this.empresa.setTipo("");
		}
	}

	public String editEmpresa() {
		log.info("Editando empresa " + this.getSelectedCNPJ());
		EmpresaDAO empresaDAO = new EmpresaDAO();
		Empresa empresa = empresaDAO.getEmpresa(this.getSelectedCNPJ());
		if (empresa != null) {
			this.empresa.setCNPJ(empresa.getCNPJ());
			this.empresa.setCNPJMatriz(empresa.getCNPJMatriz());
			this.empresa.setRazaoSocial(empresa.getRazaoSocial());
			this.empresa.setMunicipio(empresa.getMunicipio());
			this.empresa.setUf(empresa.getUf());
			this.empresa.setLogradouro(empresa.getLogradouro());
			this.empresa.setComplemento(empresa.getComplemento());
			this.empresa.setNumero(empresa.getNumero());
			this.empresa.setTipo(empresa.getTipo());
		} else {
			this.setMsg("Empresa nao encontrada!");
			log.error("Empresa nao encontrada!");
		}
		return UPDATE;
	}

	public String createEmpresa() {
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			if(this.getTipo().equals("Matriz")) {
				this.empresa.setTipo("Matriz");
			}
			else {
				this.empresa.setTipo("Filial");
				this.empresa.setCNPJMatriz(this.getSelectedCNPJ());
			}
			empresaDAO.createEmpresa(this.empresa);
			limparEmpresa();
			this.setMsg("Empresa cadastrada!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
			return INSERT;
		}
		return INDEX;
	}

	public String deleteEmpresa() {
		log.info("Excluindo empresa " + this.getSelectedCNPJ());
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			empresaDAO.deleteEmpresa(this.getSelectedCNPJ());
			limparEmpresa();
			this.setMsg("Exclu�do com sucesso!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		return INDEX;
	}

	public List<Matriz> getListaMatrizes() {
		log.info("Listando matrizes");
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			this.matrizes.clear();
			for (Matriz matriz : empresaDAO.listMatrizes()) {
				matrizes.add(matriz);
			}
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		return matrizes;
	}

	public List<Empresa> getListaEmpresas() {
		limparEmpresa();
		this.empresas.clear();
		log.info("Listando empresa");
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			for (Empresa empresa : empresaDAO.listEmpresas()) {
				empresas.add(empresa);
			}
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
		}
		return empresas;
	}

	public String updateEmpresa() {
		try {
			EmpresaDAO empresaDAO = new EmpresaDAO();
			empresaDAO.updateEmpresa(this.empresa);
			limparEmpresa();
			this.setMsg("Atualizado com sucesso!");
		} catch (Exception e) {
			this.setMsg(e.getMessage());
			log.error(e);
			return UPDATE;
		}
		return INDEX;
	}
	
	public String cadastrarEmpresa() {
		return INSERT;
	}
	
	public String voltarListagem() {
		this.empresas.clear();
		return INDEX;
	}
}
